﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class Health
{
    //1st feature: health
    public static int lives = 3;
    //to be called in DestroyByContact
    public int getLives()
    {
        return lives;
    }

    public void removeLife()
    {
        lives -= 1;
    }
    public void setLives(int i)
    {
        lives = i;
    }
    public void addLives()
    {
        lives += 1;
    }
}

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    public float speed;
    public Boundary boundary;
    public float tilt;
    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate=0.5F;

    private float nextFire=0.0F;
    private AudioSource audioSource;
    //public GUIText scoreText;
    private int score;

    private GameController gameController;



    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        score = 0;
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            GameObject clone = Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            //add to shotCount
            gameController.addShot();
            //GameObject clone = Instantiate(projectile, transform.position, transform.rotation) as GameObject;
            audioSource.Play();

        }
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.velocity = movement*speed/Time.timeScale;

        rb.position = new Vector3 (Mathf.Clamp(rb.position.x,boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax));

        rb.rotation = Quaternion.Euler(0.0f, 0.0f, moveHorizontal * -tilt);
    }



    //functions for freezing time
    public float getSpeed()
    {
        return speed;
    }
    public void setSpeed(float i)
    {
        rb.velocity = transform.forward * i;
    }


}
