﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Freezer : MonoBehaviour
{
    //feature 2: freeze moving objects (other than player & bolt)
    //GameObject slowedObj = new GameObject();
    private bool isFrozen = false;
    public float newTimeSpeed = 0.25f;
    public float slowDuration = 4f;
    //GameObject player = GameObject.FindWithTag("Player");
    //for player speed
    private float oldSpeed;
    private float newSpeed = 200;
    private float timer = 0;
    private float t1;
    public GameObject freezeParticle;
    private GameController gameController;
    // Start is called before the first frame update
    void Start()
    {
        /*
        slowedObj= GameObject.FindWithTag("freezable");
        oldSpeed = slowedObj.getSpeed();
        */
        //player.getComponent<PlayerController>();
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.unscaledDeltaTime;
        //Debug.Log(timer);
        
        //scale time back to normal
        Time.timeScale += (1f / slowDuration) *  Time.unscaledDeltaTime;
        Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
        isFrozen = false;
        //set frozen bool false if time is normal
       
        //keep track of time if it's frozen
        if (isFrozen && timer >= (t1 + slowDuration))
        {
            //Debug.Log(timer);
            Time.timeScale = 1;
            Time.fixedDeltaTime = 1;

        }
            
     
    }
    void OnTriggerEnter(Collider other)
    {
        //if the pickup object collides with player, freeze the game
        //Debug.Log(other.name);
        if (other.tag == "Player")
        {
            //add to pickup count in game controller
            gameController.addPickup();
            //remove pickup
            Destroy(gameObject);

            //increase player speed compared to slowed time
            //NOT CURRENTLY FUNCTIONAL
            //oldSpeed = other.gameObject.GetComponent<PlayerController>().getSpeed();
            //Debug.Log(oldSpeed);
            //other.gameObject.GetComponent<PlayerController>().setSpeed(newSpeed);
            if (isFrozen == false) {
                isFrozen = true;
                //slow everything

                //not using particle effect on pickups, slow & collisions messed up
                //Instantiate(freezeParticle,other.transform.position,freezeParticle.transform.rotation);
                freezeTime(); }
            
           
        }
    }
    public void freezeTime()
    {
        t1 = timer;
        Time.timeScale = newTimeSpeed;
        Time.fixedDeltaTime = Time.timeScale * .02f;
       isFrozen = false;
    }
    

}
