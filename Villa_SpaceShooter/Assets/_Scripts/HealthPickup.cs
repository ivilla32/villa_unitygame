﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{
    private GameController gameController;
    private static int hp;
    // Start is called before the first frame update
    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
        hp = gameController.GetHP();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {
        //if the pickup object collides with player, freeze the game
        Debug.Log(other.name);
        if (other.tag == "Player")
        {
            //remove pickup
            Destroy(gameObject);
            //adjust health
            hp = gameController.GetHP();
            Debug.Log("HP: " + hp);
            gameController.AddHP();
            hp = gameController.GetHP();
            Debug.Log("New HP: " + hp);
            /*
            Debug.Log(other.gameObject.GetComponent<Health>().getLives());
            other.gameObject.GetComponent<Health>().addLives();
            Debug.Log(other.gameObject.GetComponent<Health>().getLives());
            */
        }
    }
}
