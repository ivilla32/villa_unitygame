﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public GameObject hazard;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    //variables for wall hazard spawn
    public GameObject wall;
    public Vector3 wallSpawnValues;
    public int wallCount;
    public float wallSpawnWait;
    public float wallStartWait;
    public float wallWaveWait;

    //hp text
    private int hp;

    public Text scoreText;
    private int score;

    public Text restartText;
    public Text gameOverText;

    private bool gameOver;
    private bool restart;

    //tracking pickups/accuracy
    private int pickupCount = 0;
    private int shotCount = 0;
    private double shotAccuracy;
    public Text accuracyText;
    public Text pickupText;
    

    // Start is called before the first frame update
    void Start()
    {
        gameOver = false;
        restart = false;
        hp = 3;
        restartText.text = "HP: 3";
        gameOverText.text = "";
        accuracyText.text = "";
        pickupText.text = "";
        score = 0;
        UpdateScore();
        StartCoroutine(SpawnWaves());

        
    }

    // Update is called once per frame
    void Update()
    {
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        //wall spawn yield
        yield return new WaitForSeconds(wallSpawnWait);


        while (true)
        {
            //hazard spwan
            for (int i = 0; i < hazardCount; i++)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                //rotation for asteroid
                Quaternion spawnRotation = Quaternion.identity;
                //rotation for cop car
                //Quaternion spawnRotation = hazard.transform.rotation;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

            //wall spawn
            for (int i = 0; i < wallCount; i++)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(-wallSpawnValues.x, wallSpawnValues.x), wallSpawnValues.y, wallSpawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                //Instantiate(wall, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(wallSpawnWait);
            }
            yield return new WaitForSeconds(wallWaveWait);

            if (gameOver)
            {
                restartText.text = "Press 'R' for Restart";
                restart = true;
                break;
            }
        }
    }

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
        //Debug.Log("Score: " + score);
    }

    public void GameOver()
    {
        //calculate accuracy
        if (score != 0)
        {
            shotAccuracy = (score*100)/shotCount;
        }
        else
        {
            shotAccuracy = 0.00;
        }
        gameOverText.text = "Game Over!";
        accuracyText.text = "Shot Accuracy: " + shotAccuracy.ToString()+"%";
        pickupText.text="Pickups Collected: " +pickupCount.ToString();
        gameOver = true;
    }

    //hp functions
    public void AddHP()
    {
        hp += 1;
    }
    public int GetHP()
    {
        return hp;
    }
    public void RemoveHP()
    {
        hp -= 1;
        restartText.text = "HP: " + hp;
    }

    //accuracy/pickup count functions
    public void addShot()
    {
        shotCount += 10;
    }
    public void addPickup()
    {
        pickupCount += 1;
    }
}
