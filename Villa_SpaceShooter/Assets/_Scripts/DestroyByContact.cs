﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue;
    private GameController gameController;
    private static Health playerHealth = new Health(); //for using health functions in playercontroller
    private static int hp;
    //private GameObject player;
    

    // Start is called before the first frame update
    void Start()
    {
        
        //player = GameObject.FindWithTag("Player");
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            //Debug.Log("Cannot find 'GameController' script");
        }
        hp = gameController.GetHP();
    }

    // Update is called once per frame
    void Update()
    {
        //hp = playerHealth.getLives();
        //Debug.Log("HP: " + hp);
    }
    void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.name);
        if (other.tag == "Boundary")
        {
            return;
        }
        
        else if (other.tag == "Player")
        {
            hp = playerHealth.getLives();
            Debug.Log("HP Before: "+hp);
            //if the object collided is a health pickup
            /*
            if (gameObject.tag == "health")
            {
                //hp += 1;
                gameController.AddHP();
                
            }
            //otherwise hurt player
            else if (gameObject.tag != "health")
            {*/
            //if player has more than 1 hp
                if (hp > 1)
                {
                    hp -= 1;
                    playerHealth.removeLife();
                    gameController.RemoveHP();
                }
                //if player is on last hp
                else
                {
                    //destroy player
                    Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
                    gameController.GameOver();
                    Destroy(other.gameObject);
                    gameController.RemoveHP();
            }
            
            
            //hp = playerHealth.getLives();
            //hp = gameController.GetHP();
            Debug.Log("HP After: " + hp);
            //destroy asteroid
            Instantiate(explosion, transform.position, transform.rotation);
            Destroy(gameObject);
        }
        //if it collides w/ anything else than wall or freezer (AKA bolt)
        else if (other.tag != "wall")
        {
            
            if (other.tag != "freezer")
            {
                gameController.AddScore(scoreValue);
                Instantiate(explosion, transform.position, transform.rotation);
                Destroy(other.gameObject);
                Destroy(gameObject);
                
            }
        }
        
    }
}
