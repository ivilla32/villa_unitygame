﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Mover : MonoBehaviour
{
    private Rigidbody rb;
    public float speed;
    
    // Start is called before the first frame update
    void Start()
    {
        
        rb = GetComponent<Rigidbody>();

        //adjusting mover axis
        //new Vector3(0, 0, 10);
        ///*
        Quaternion tilter = Quaternion.Euler(0, 0, 90);
        if (gameObject.tag == "asteroid")
        {
            gameObject.transform.rotation = Quaternion.Slerp(transform.rotation, tilter, 1);
        }
        //*/

        rb.velocity = transform.forward * speed;


    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
}
